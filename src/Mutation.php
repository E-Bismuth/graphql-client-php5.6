<?php

namespace GraphQL;

use GraphQL\Exception\ArgumentException;
use GraphQL\Exception\InvalidSelectionException;
use GraphQL\Util\StringLiteralFormatter;

/**
 * Class Query
 *
 * @package GraphQL
 */
class Mutation extends Query
{

    /**
     * @return string
     * @throws \Exception
     */
    public function __toString()
    {
        $queryFormat = ($this->asSelectionSet())? "%s%s {\n%s\n}" : "%s%s%s";
        if (!$this->isNested) {
            $queryFormat = "mutation {\n" . $queryFormat . "\n}";
        }
        $argumentsString    = $this->constructArguments();
        $selectionSetString = $this->constructSelectionSet();

        return sprintf($queryFormat, $this->object, $argumentsString, $selectionSetString);
    }
}
